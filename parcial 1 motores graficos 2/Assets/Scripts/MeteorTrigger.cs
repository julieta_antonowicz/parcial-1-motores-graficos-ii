using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorTrigger : MonoBehaviour
{
    public GameObject meteorPrefab;  // Prefab del meteorito
    public float spawnInterval = 2f;  // Intervalo de generación de meteoritos
    public GameObject objetoPosicion;

    private bool isPlayerInside = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isPlayerInside = true;
            InvokeRepeating("SpawnMeteor", 0f, spawnInterval);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isPlayerInside = false;
            CancelInvoke("SpawnMeteor");
        }
    }

    private void SpawnMeteor()
    {
        if (isPlayerInside)
        {
            Vector3 GenerarPosicion= objetoPosicion.transform.position;

            // Instanciar el prefab del meteorito en la posición generada
            Instantiate(meteorPrefab, GenerarPosicion, Quaternion.identity);
        }
    }
}