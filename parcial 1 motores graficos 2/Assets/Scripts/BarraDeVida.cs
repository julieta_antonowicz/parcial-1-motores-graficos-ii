using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeVida : MonoBehaviour
{
    public Image barraDeVida;
    public static float vidaActual;
    public float vidaMaxima;

    void Update()
    {
        barraDeVida.fillAmount = vidaActual / vidaMaxima;
    }
}
