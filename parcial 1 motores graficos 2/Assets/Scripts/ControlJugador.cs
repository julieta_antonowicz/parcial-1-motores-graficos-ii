using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float jumpForce = 5f;
    private bool isJumping = false;
    public bool restarVidaFrio = false;
    private float tiempoEnFrio = 0;
    public GameObject CamaraPerdiste;
    public bool agarroNeptuno = false;
    public bool agarroUrano = false;
    public static bool agarroSaturno = false;
    public bool agarroMarte = false;
    private Rigidbody rb;
    public float lowGravity;
    public bool gravedadMarteHabilitar;

    private void Update()
    {
        float moveX = Input.GetAxis("Horizontal") * moveSpeed;
        Vector3 newPosition = transform.position + new Vector3(moveX * Time.deltaTime, 0f, 0f);
        transform.position = newPosition;

        if (Input.GetButtonDown("Jump") && !isJumping)
        {
            GetComponent<Rigidbody>().AddForce(new Vector3(0f, jumpForce, 0f), ForceMode.Impulse);
            isJumping = true;
        }

        if (restarVidaFrio)
        {
            tiempoEnFrio=Time.deltaTime;
            for(int i = 0; i < tiempoEnFrio; i++)
            {
                RestarVida();
            }
        }

        if(BarraDeVida.vidaActual <= 0 )
        {
            CamaraPerdiste.SetActive(true);

            if (Input.GetKeyDown(KeyCode.R))
            {
                CamaraPerdiste.SetActive(false);
                BarraDeVida.vidaActual = 10000;
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(currentSceneIndex);
        }
    }
    private void FixedUpdate()
    {
        if (agarroMarte)
        {
            gravedadMarte();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Neptuno"))
        {
            Destroy(other.gameObject);
            agarroNeptuno = true;
        }
        if (other.gameObject.CompareTag("Urano"))
        {
            Destroy(other.gameObject);
            agarroUrano = true;
        }
        if (other.gameObject.CompareTag("Saturno"))
        {
            Destroy(other.gameObject);
            agarroSaturno= true;
        }
        if (other.gameObject.CompareTag("Marte"))
        {
            Destroy(other.gameObject);
            agarroMarte = true;
        }
        if (other.gameObject.CompareTag("anillosSaturno"))
        {
            transform.SetParent(other.transform);
        }
        if (other.gameObject.CompareTag("Meteorito") && !agarroUrano)
        {
            BarraDeVida.vidaActual -= 1000;
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("ZonaFrio") && !agarroNeptuno)
        {
            restarVidaFrio=true;
           
        }
        if (other.gameObject.CompareTag("habilitarTierra"))
        {
            SceneManager.LoadScene("Escena Final");
        }
        else if (other.gameObject.CompareTag("fueraDeFrio"))
        {
            restarVidaFrio=false;

        }

    }
    private void OnTriggerExit(Collider other)
    {
        transform.SetParent(null);
    }
    public void RestarVida()
    {
        BarraDeVida.vidaActual -= 10;
    }
    public void gravedadMarte()
    {
        rb.useGravity = false;
        rb.AddForce(Vector3.down * lowGravity, ForceMode.Acceleration);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            isJumping = false;
        }
    }
    private void Start()
    {
        BarraDeVida.vidaActual = 10000;
        CamaraPerdiste.SetActive(false);
        rb = GetComponent<Rigidbody>();
    }
}