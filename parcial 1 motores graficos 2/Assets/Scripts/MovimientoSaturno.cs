using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoSaturno : MonoBehaviour
{
    public float rotationSpeed = 10f;  // Velocidad de rotaci�n del objeto

    private void Update()
    {
        if (ControlJugador.agarroSaturno)
        {
            girar();
        }
    }
    public void girar()
    {
        transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
    }
}
